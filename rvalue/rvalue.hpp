#include <ostream>

// http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n2118.html

class A

{

public:

    A(int i) : i_(i) {}

    int get() const {
        return i_;
    }

private:

    int i_;

};

std::ostream&

operator<< (std::ostream& os, const A& a)

{

    return os << a.get();

}
