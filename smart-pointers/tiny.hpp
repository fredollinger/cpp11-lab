#ifndef __CPP11_LAB_TINY_HPP_
#define __CPP11_LAB_TINY_HPP_

namespace cpp11lab {

class Tiny
{
public:
  Tiny();
  virtual ~Tiny();
};

} // END class Tiny

#endif // __CPP11_LAB_TINY_HPP_
