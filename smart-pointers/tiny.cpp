#include "tiny.hpp"
#include <iostream>

namespace cpp11lab {
Tiny::Tiny() {
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}

Tiny::~Tiny() {
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}
} // END namespace cpp11lab
