#include "tiny.hpp"

#include <memory>

using namespace cpp11lab;

void frob() {
    std::shared_ptr<Tiny> p1(std::make_shared<Tiny>());
    return;
}

int main() {
    frob();

    while(1) {}
    return 0;
}
